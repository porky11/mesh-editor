use crate::{command::Command, convert::Convert as _};

use femto_mesh::{
    Animation, AnimationFrame, AnimationState, FillStyle, IndexedPath, PathList, SelectionEditor,
    StrokeStyle, VectorMesh, VectorMeshExtensions as _,
};
use femtovg::{Canvas, LineCap, LineJoin, Paint, Path, Renderer};
use ga2::Vector;
use gapp::{Render, Update};
use gapp_winit::WindowInput;
use rfd::{FileDialog, MessageButtons, MessageDialog, MessageLevel};
use simple_color::Color;
use touch_selection::{Action, GrabState, ObjectGrabState, Selection};
use winit::{
    event::{
        ElementState, KeyboardInput, ModifiersState, MouseButton, VirtualKeyCode, WindowEvent,
    },
    event_loop::ControlFlow,
};

use std::{collections::HashSet, path::PathBuf, sync::mpsc::Receiver};

struct Grid {
    width: usize,
    height: usize,
    center: (usize, usize),
}

fn render_point<R: Renderer>(
    canvas: &mut Canvas<R>,
    x: f32,
    y: f32,
    r: f32,
    paint: Paint,
    text: Option<&str>,
) {
    let mut path = Path::new();
    path.circle(x, y, r);
    canvas.fill_path(&path, &paint);
    if let Some(text) = text {
        let _ = canvas.fill_text(x + r, y + r, text, &paint);
    }
}

impl Grid {
    fn center(&self) -> Vector<f32> {
        let (x, y) = self.center;
        let cx = x as f32 - self.width as f32 / 2.0;
        let cy = y as f32 - self.height as f32 / 2.0;
        Vector::new(cx, -cy)
    }

    fn render<R: Renderer>(&self, canvas: &mut Canvas<R>, zoom: f32) {
        let zoom = zoom / 2.0;

        for x in 0..=self.width {
            let x = (2.0 * x as f32 - self.width as f32) * zoom;
            let y0 = -(self.height as f32) * zoom;
            let y1 = self.height as f32 * zoom;

            let mut path = Path::new();
            path.move_to(x, y0);
            path.line_to(x, y1);
            canvas.stroke_path(&path, &Paint::color(Color::gray(0x80).convert()));
        }

        for y in 0..=self.height {
            let x0 = -(self.width as f32) * zoom;
            let x1 = self.width as f32 * zoom;
            let y = (2.0 * y as f32 - self.height as f32) * zoom;

            let mut path = Path::new();
            path.move_to(x0, y);
            path.line_to(x1, y);
            canvas.stroke_path(&path, &Paint::color(Color::gray(0x80).convert()));
        }

        let (x, y) = self.center;
        let cx = (2.0 * x as f32 - self.width as f32) * zoom;
        let cy = (2.0 * y as f32 - self.height as f32) * zoom;

        render_point(
            canvas,
            cx,
            cy,
            zoom / 2.0,
            Paint::color(Color::gray(0x40).convert()),
            None,
        );
    }
}

pub struct Editor {
    cursor: Vector<f32>,
    size: (f32, f32),
    grid: Grid,
    mesh: VectorMesh,
    mirror_pairs: Vec<usize>,
    animation: Animation,
    animation_state: AnimationState,
    mode: Mode,
    selection: Selection<Vector<f32>, HashSet<usize>>,
    selection_editor: SelectionEditor,
    mesh_path: Option<PathBuf>,
    animation_path: Option<PathBuf>,
    receiver: Receiver<Command>,
}

enum Mode {
    Editing,
    Animating,
}

impl Editor {
    pub fn new(receiver: Receiver<Command>) -> Self {
        let points = vec![
            Vector::new(0.0, 0xC as f32),
            Vector::new(-6.0, 8.0),
            Vector::new(6.0, 8.0),
            Vector::new(-8.0, 0.0),
            Vector::new(8.0, 0.0),
            Vector::new(0.0, 0x14 as f32),
            Vector::new(-4.0, 0x14 as f32),
            Vector::new(4.0, 0x14 as f32),
            Vector::new(-6.0, 0xC as f32),
            Vector::new(6.0, 0xC as f32),
            Vector::new(0.0, 0x20 as f32),
            Vector::new(0.0, 0x18 as f32),
            Vector::new(-6.0, 0x20 as f32),
            Vector::new(6.0, 0x20 as f32),
            Vector::new(-4.0, 0x18 as f32),
            Vector::new(4.0, 0x18 as f32),
        ];

        let point_count = points.len();

        fn collect<I: Iterator, const SIZE: usize>(iter: I) -> [I::Item; SIZE]
        where
            I::Item: Copy + Default,
        {
            let mut result = [I::Item::default(); SIZE];
            for (i, item) in iter.enumerate() {
                result[i] = item;
            }
            result
        }

        let [
            hip,
            knee_r,
            knee_l,
            foot_r,
            foot_l,
            chest,
            elbow_r,
            elbow_l,
            hand_r,
            hand_l,
            head_t,
            head_b,
            head_tr,
            head_tl,
            head_br,
            head_bl,
        ] = collect(0..point_count);

        let mut mesh = VectorMesh::new(points.clone(), PathList::new());

        let stroke_style = StrokeStyle::color(Color::BLACK)
            .with_line_cap(LineCap::Round)
            .with_line_join(LineJoin::Round);

        let mut leg_path_l = IndexedPath::stroke(stroke_style.clone());
        leg_path_l.move_to(hip);
        leg_path_l.quad_to(knee_l, foot_l);
        mesh.add_path(leg_path_l);

        let mut leg_path_r = IndexedPath::stroke(stroke_style.clone());
        leg_path_r.move_to(hip);
        leg_path_r.quad_to(knee_r, foot_r);
        mesh.add_path(leg_path_r);

        let mut belly_path = IndexedPath::stroke(stroke_style.clone());
        belly_path.move_to(hip);
        belly_path.line_to(chest);
        belly_path.line_to(head_b);
        mesh.add_path(belly_path);

        let mut arm_path_l = IndexedPath::stroke(stroke_style.clone());
        arm_path_l.move_to(chest);
        arm_path_l.quad_to(elbow_l, hand_l);
        mesh.add_path(arm_path_l);

        let mut arm_path_r = IndexedPath::stroke(stroke_style.clone());
        arm_path_r.move_to(chest);
        arm_path_r.quad_to(elbow_r, hand_r);
        mesh.add_path(arm_path_r);

        let mut head_path = IndexedPath::new(FillStyle::color(Color::gray(0x40)), stroke_style);
        head_path.move_to(head_b);
        head_path.bezier_to(head_bl, head_tl, head_t);
        head_path.bezier_to(head_tr, head_br, head_b);
        mesh.add_path(head_path);

        let mirror_pairs = vec![knee_r, foot_r, elbow_r, hand_r, head_tr, head_br];

        Self {
            cursor: Vector::new(0.0, 0.0),
            size: (0.0, 0.0),
            grid: Grid {
                width: 0x10,
                height: 0x20,
                center: (8, 0),
            },
            mesh,
            mirror_pairs,
            animation: Animation::new(vec![AnimationFrame::new(points, 1.0)]),
            animation_state: AnimationState::default(),
            mode: Mode::Editing,
            selection: Selection::new(HashSet::new()),
            selection_editor: SelectionEditor::new(),
            mesh_path: None,
            animation_path: None,
            receiver,
        }
    }

    fn zoom(&self) -> f32 {
        self.size.0.min(self.size.1) / self.grid.width.max(self.grid.height) as f32
    }

    fn mesh_dialog(&self) -> FileDialog {
        let dialog = FileDialog::new().add_filter("Mesh", &["vgmesh"]);

        let mut path = if let Some(path) = &self.mesh_path {
            path.clone()
        } else if let Ok(path) = std::env::current_dir() {
            path
        } else {
            return dialog;
        };

        if path.is_file() {
            if let Some(parent) = path.parent() {
                path = parent.to_path_buf();
            }
        }

        dialog.set_directory(path)
    }

    fn animation_dialog(&self) -> FileDialog {
        let dialog = FileDialog::new().add_filter("Animation", &["vganim"]);

        let mut path = if let Some(path) = &self.animation_path {
            path.clone()
        } else if let Ok(path) = std::env::current_dir() {
            path
        } else {
            return dialog;
        };

        if path.is_file() {
            if let Some(parent) = path.parent() {
                path = parent.to_path_buf();
            }
        }

        dialog.set_directory(path)
    }
}

impl Update for Editor {
    fn update(&mut self, timestep: f32) {
        while let Ok(command) = self.receiver.try_recv() {
            match command {
                Command::Point(point) => self.mesh.points.push(point),
            }
        }

        let point = self.grid.center() + self.cursor / self.zoom();

        if let Some(grab) = &mut self.selection.grab {
            if grab.moved.is_some() {
                grab.time += timestep;
                if grab.time >= 0.3 {
                    grab.hold(&self.selection_editor, &mut self.mesh);
                }
            } else if let GrabState::Object(ObjectGrabState {
                action: Action::Move,
                ..
            }) = grab.state
            {
                let grab_pos0 = grab.pos;
                grab.pos = point;
                let grab_pos1 = grab.pos;
                for &selected in &self.selection {
                    let point = &mut self.mesh.points[selected];
                    let grab_offset = *point - grab_pos0;
                    *point = grab_pos1 + grab_offset
                }
            }
        }

        match self.mode {
            Mode::Animating => {
                self.animation_state.update(timestep, &self.animation);
                self.mesh
                    .set_animation(&self.animation, &self.animation_state);
            }
            Mode::Editing => {}
        }
    }
}

impl<R: Renderer> Render<Canvas<R>> for Editor {
    fn render(&self, canvas: &mut Canvas<R>) {
        canvas.clear_rect(
            0,
            0,
            canvas.width(),
            canvas.height(),
            Color::WHITE.convert(),
        );

        let zoom = self.zoom();

        self.grid.render(canvas, zoom);

        canvas.save();
        let center = self.grid.center();
        canvas.translate(-center.x * zoom, -center.y * zoom);
        canvas.scale(zoom, zoom);
        self.mesh.render(canvas);
        for (i, &Vector { x, y }) in self.mesh.points.iter().enumerate() {
            let selected = self.selection.selected.contains(&i);
            let color = if selected { Color::YELLOW } else { Color::RED };
            render_point(
                canvas,
                x,
                y,
                0.25,
                Paint::color(color.convert()),
                Some(&format!("{i}")),
            );
        }
        canvas.restore();

        canvas.flush();
    }
}

fn save_file_failed(path: &PathBuf) {
    MessageDialog::new()
        .set_level(MessageLevel::Error)
        .set_title("Saving file failed!")
        .set_description(&format!("The file \"{path:?}\" could not be saved")[..])
        .set_buttons(MessageButtons::Ok)
        .show();
}

fn load_file_failed(path: &PathBuf) {
    MessageDialog::new()
        .set_level(MessageLevel::Error)
        .set_title("Loading file failed!")
        .set_description(&format!("The file \"{path:?}\" could not be loaded")[..])
        .set_buttons(MessageButtons::Ok)
        .show();
}

impl<R: Renderer> WindowInput<ModifiersState, Canvas<R>> for Editor {
    fn input(
        &mut self,
        event: &WindowEvent,
        control_flow: &mut ControlFlow,
        context_modifiers: &mut ModifiersState,
        canvas: &mut Canvas<R>,
    ) {
        match event {
            &WindowEvent::ModifiersChanged(modifiers) => *context_modifiers = modifiers,

            WindowEvent::CloseRequested => *control_flow = ControlFlow::Exit,

            WindowEvent::Resized(size) => {
                let (w, h) = (size.width, size.height);
                self.size = (w as f32, h as f32);
                canvas.set_size(w, h, 1.0);
                canvas.reset_transform();
                canvas.translate(w as f32 / 2.0, h as f32 / 2.0);
                canvas.scale(1.0, -1.0);
            }

            WindowEvent::CursorMoved { position, .. } => {
                let (x, y) = (
                    position.x as f32 - self.size.0 / 2.0,
                    position.y as f32 - self.size.1 / 2.0,
                );
                self.cursor = Vector::new(x, -y);

                self.selection.on_moved(self.cursor, &self.selection_editor)
            }

            WindowEvent::MouseInput { state, button, .. } => {
                let point = self.grid.center() + self.cursor / self.zoom();

                use ElementState::*;
                match state {
                    Pressed => {
                        use MouseButton::*;
                        let (Left | Right) = button else {
                            return;
                        };

                        self.selection.press(
                            point,
                            self.cursor,
                            &self.selection_editor,
                            &self.mesh,
                        );

                        let Right = button else {
                            return;
                        };

                        self.selection
                            .grab
                            .as_mut()
                            .expect("Has just been assigned by pressing")
                            .hold(&self.selection_editor, &mut self.mesh);
                    }
                    Released => {
                        self.selection
                            .release(point, &self.selection_editor, &mut self.mesh)
                    }
                }
            }

            WindowEvent::KeyboardInput {
                input:
                    KeyboardInput {
                        virtual_keycode: Some(keycode),
                        state: ElementState::Pressed,
                        ..
                    },
                ..
            } => {
                use VirtualKeyCode::*;
                if *keycode == Space {
                    self.mode = match self.mode {
                        Mode::Editing => Mode::Animating,
                        Mode::Animating => Mode::Editing,
                    };
                    if matches!(self.mode, Mode::Editing) {
                        for Vector { x, y } in &mut self.mesh.points {
                            *x = x.round();
                            *y = y.round();
                        }
                    }
                }

                if !context_modifiers.ctrl() {
                    return;
                }

                match keycode {
                    A => {
                        self.animation_state.next_frame(&self.animation);
                        self.animation.frames.insert(
                            self.animation_state.index,
                            AnimationFrame::new(self.mesh.points.clone(), 1.0),
                        );
                    }
                    R => {
                        self.animation_state.next_frame(&self.animation);
                        self.animation.frames[self.animation_state.index] =
                            AnimationFrame::new(self.mesh.points.clone(), 1.0);
                    }
                    C => {
                        self.animation = Animation::new(vec![AnimationFrame::new(
                            self.mesh.points.clone(),
                            1.0,
                        )]);
                        self.animation_state = AnimationState::default();
                    }
                    D => {
                        self.animation_state.next_frame(&self.animation);
                        if self.animation.frames.len() == 1 {
                            self.animation.frames[0] =
                                AnimationFrame::new(self.mesh.points.clone(), 1.0);
                        } else {
                            self.animation.frames.remove(self.animation_state.index);
                            if self.animation_state.index == self.animation.frames.len() {
                                self.animation_state.index = 0;
                            }
                        }
                    }
                    S => match self.mode {
                        Mode::Animating => {
                            if self.animation_path.is_none() || context_modifiers.shift() {
                                if let Some(path) = self.animation_dialog().save_file() {
                                    self.animation_path = Some(path);
                                }
                            }

                            if let Some(path) = &self.animation_path {
                                if self.animation.save(path).is_err() {
                                    save_file_failed(path)
                                }
                            }
                        }
                        Mode::Editing => {
                            if self.mesh_path.is_none() || context_modifiers.shift() {
                                if let Some(path) = self.mesh_dialog().save_file() {
                                    self.mesh_path = Some(path);
                                }
                            }
                            if let Some(path) = &self.mesh_path {
                                if self.mesh.save(path).is_err() {
                                    save_file_failed(path)
                                }
                            }
                        }
                    },
                    M => match self.mode {
                        Mode::Animating => {
                            for frame in &mut self.animation.frames {
                                for Vector { x, .. } in &mut frame.points {
                                    *x = -*x;
                                }
                                for &pair in &self.mirror_pairs {
                                    frame.points.swap(pair, pair + 1);
                                }
                            }
                        }
                        Mode::Editing => {
                            for Vector { x, .. } in &mut self.mesh.points {
                                *x = -*x;
                            }
                            for &pair in &self.mirror_pairs {
                                self.mesh.points.swap(pair, pair + 1);
                            }
                        }
                    },
                    O => match self.mode {
                        Mode::Animating => {
                            if self.animation_path.is_none() || !context_modifiers.shift() {
                                if let Some(path) = self.animation_dialog().pick_file() {
                                    self.animation_path = Some(path);
                                }
                            }
                            if let Some(path) = &self.animation_path {
                                if let Ok(animation) = Animation::load(path) {
                                    self.animation = animation;
                                } else {
                                    load_file_failed(path);
                                }
                            }
                        }
                        Mode::Editing => {
                            if self.mesh_path.is_none() || !context_modifiers.shift() {
                                if let Some(path) = self.mesh_dialog().pick_file() {
                                    self.mesh_path = Some(path);
                                }
                            }
                            if let Some(path) = &self.mesh_path {
                                if let Ok(mesh) = VectorMesh::load(path) {
                                    self.mesh = mesh;
                                } else {
                                    load_file_failed(path);
                                }
                            }
                        }
                    },
                    _ => (),
                }
            }

            _ => (),
        }
    }
}
