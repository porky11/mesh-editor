use std::{
    fs::File,
    io::{BufRead, BufReader, Write},
    path::{Path, PathBuf},
    sync::mpsc::Sender,
};

use ga2::Vector;

fn split_line(line: &str) -> impl Iterator<Item = &str> {
    let mut in_string = false;
    let split = move |c: char| {
        if c == '"' {
            in_string = !in_string;
            return true;
        }

        if in_string {
            return false;
        };

        c.is_whitespace()
    };

    line.split(split).filter(|s| !s.is_empty())
}

pub enum Command {
    Point(Vector<f32>),
}

impl Command {
    pub fn new<'a>(action: &'a str, params: impl Iterator<Item = &'a str>) -> Vec<Self> {
        let mut result = Vec::new();
        Self::add(action, params, &mut result, &mut Vec::new());
        result
    }

    fn add<'a>(
        action: &str,
        mut params: impl Iterator<Item = &'a str>,
        result: &mut Vec<Self>,
        load_cache: &mut Vec<PathBuf>,
    ) {
        match action {
            "load" => {
                for path in params {
                    let path: PathBuf = path.into();
                    if load_cache.contains(&path) {
                        eprintln!("File {path:?} tries to load itself!");
                        continue;
                    }

                    load_cache.push(path.clone());

                    Self::load(&path, result, load_cache);

                    load_cache.pop();
                }
            }
            "point" => {
                let (Some(x), Some(y), None) = (params.next(), params.next(), params.next()) else {
                    eprintln!("Invalid parameter count for point");
                    return;
                };

                let Ok(x) = x.parse() else {
                    eprintln!("Error parsing x parameter");
                    return;
                };
                let Ok(y) = y.parse() else {
                    eprintln!("Error parsing y parameter");
                    return;
                };

                result.push(Self::Point(Vector::new(x, y)));
            }
            _ => eprintln!("Unknown action: {action}"),
        };
    }

    fn load(path: &Path, result: &mut Vec<Self>, load_cache: &mut Vec<PathBuf>) {
        let Ok(file) = File::open(path) else {
            eprintln!("Could not open file {path:?}");
            return;
        };

        for line in BufReader::new(file).lines() {
            let Ok(line) = line else {
                eprintln!("Error while reading file");
                break;
            };

            let mut command = split_line(&line);

            if let Some(action) = command.next() {
                Self::add(action, command, result, load_cache);
            }
        }
    }
}

pub fn command_loop(sender: Sender<Command>) {
    let mut stdout = std::io::stdout();
    let stdin = std::io::stdin();

    loop {
        print!("> ");
        let _ = stdout.flush();

        let mut line = String::new();
        if stdin.read_line(&mut line).is_err() {
            eprintln!("Error reading line!");
            continue;
        }

        let mut command = split_line(&line);

        let Some(action) = command.next() else {
            continue;
        };

        match action {
            "help" => {
                eprintln!("Available commands: help, exit");
                continue;
            }
            "exit" => {
                eprintln!("Exiting command loop");
                return;
            }
            _ => (),
        }

        for command in Command::new(action, command) {
            if sender.send(command).is_err() {
                eprintln!("Sending command failed");
            }
        }
    }
}
